/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dv.ExtractData.controller;

import com.dv.ExtractData.helpers.ExcelHelper;
import com.dv.ExtractData.models.DataExtract;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.dv.ExtractData.utils.ServiceConstants.PATH_BASE;
import static com.dv.ExtractData.utils.ServiceConstants.PATH_LOAD_ARCHIVE;

/**
 *
 * @author danny
 */
@RestController
@RequestMapping(PATH_BASE)
class ExtracDataController {
    @PostMapping(PATH_LOAD_ARCHIVE)
    @CrossOrigin(origins = "http://ec2-54-175-45-98.compute-1.amazonaws.com:5000")
    public ResponseEntity<DataExtract> cargarArchivo(@RequestParam("file") MultipartFile file){
        if (ExcelHelper.esExcel(file)) {
            try {
                 return ResponseEntity.ok().body(ExcelHelper.extractData(file.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
