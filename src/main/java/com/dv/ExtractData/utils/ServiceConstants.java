package com.dv.ExtractData.utils;

public class ServiceConstants {
    public static final String VERSION = "1.0";
    public static final String PATH_BASE = "/api/" + VERSION + "/extractData/";
    public static final String PATH_LOAD_ARCHIVE = "load";
}
