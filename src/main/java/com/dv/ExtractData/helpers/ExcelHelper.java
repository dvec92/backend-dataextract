package com.dv.ExtractData.helpers;

import com.dv.ExtractData.models.DataExtract;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static Integer cellIni = 2;
    public static Integer cellFin = 6;

    public static boolean esExcel(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }


    public static DataExtract extractData(InputStream is) throws IOException {
        DataExtract datos = new DataExtract();
        Workbook workbook = new XSSFWorkbook(is);
        Sheet hoja = workbook.getSheetAt(0);
        datos.setTitulo(hoja.getRow(0).getCell(0).getStringCellValue());
        datos.setPeriodos(hoja.getRow(1).getCell(0).getStringCellValue());
        datos.setMoneda(hoja.getRow(2).getCell(0).getStringCellValue());

        Map<String, BigDecimal> inversion = new HashMap<>();
        inversion.put(hoja.getRow(7).getCell(0).getStringCellValue(), BigDecimal.valueOf(hoja.getRow(7).getCell(1).getNumericCellValue()).setScale(2,RoundingMode.HALF_UP));
        inversion.put(hoja.getRow(8).getCell(0).getStringCellValue(), BigDecimal.valueOf(hoja.getRow(8).getCell(1).getNumericCellValue()).setScale(2,RoundingMode.HALF_UP));
        inversion.put(hoja.getRow(9).getCell(0).getStringCellValue(), BigDecimal.valueOf(hoja.getRow(9).getCell(1).getNumericCellValue()).setScale(2,RoundingMode.HALF_UP));
        datos.setInversion(inversion);

        List<String> aniosFlujoDeCaja = new ArrayList<>();
        Row flujoCaja = hoja.getRow(14);
        for (int cc = cellIni; cc <= cellFin; cc++){
            Cell cell = flujoCaja.getCell(cc);
            if (cell.getCellType().equals(CellType.BLANK)) {
                aniosFlujoDeCaja.add(null);
            }else if (cell.getCellType().equals(CellType.STRING)){
                aniosFlujoDeCaja.add(cell.getStringCellValue());
            }else if (cell.getCellType().equals(CellType.NUMERIC)){
                aniosFlujoDeCaja.add(String.valueOf((int)cell.getNumericCellValue()));
            }
        }
        datos.setAniosFlujoDeCaja(aniosFlujoDeCaja);

        datos.setVentas(extractData(18, 19, hoja));
        datos.setIngresos(extractData(24, 25, hoja));
        datos.setTotalIngresos(extractTotales(26, hoja));
        datos.setGastos(extractData(31, 39, hoja));
        datos.setOtrosRubros(extractData(41, 46, hoja));
        datos.setTotalGastos(extractTotales(48, hoja));
        datos.setFlujoCajaAntesImpuestos(extractTotales(51, hoja));
        datos.setImpuestoSobreRenta(extractTotales(54, hoja));
        datos.setFlujoNeto(extractTotales(57, hoja));
        datos.setEfectivoDisponible(extractTotales(60, hoja));
        datos.setClsificacionIngresos(sumarizar(datos.getIngresos()));
        datos.setVentasPorAnio(ventasPorAnio(datos.getAniosFlujoDeCaja(),datos.getVentas()));
        workbook.close();
        return datos;
    }

    private static List<BigDecimal> extractTotales(Integer rw, Sheet sheet){
        List<BigDecimal> lista = new ArrayList<>();
        Row row = sheet.getRow(rw);
        for (int cl = cellIni; cl <= cellFin; cl++){
            Cell cell = row.getCell(cl);
            if (cell.getCellType().equals(CellType.BLANK)){
                lista.add(BigDecimal.ZERO);
            }else if (cell.getCellType().equals(CellType.NUMERIC)){
                lista.add(BigDecimal.valueOf(cell.getNumericCellValue()).setScale(2,RoundingMode.HALF_UP));
            }else if (cell.getCellType().equals(CellType.FORMULA)){
                lista.add(BigDecimal.valueOf(cell.getNumericCellValue()).setScale(2, RoundingMode.HALF_UP));
            }
        }
        return lista;
    }

    private static Map<String, BigDecimal> sumarizar(Map<String, List<BigDecimal>> montos) {
        Map<String, BigDecimal> sumas = new HashMap<>();
        sumas = montos.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().stream().reduce(BigDecimal.ZERO, BigDecimal::add)
                ));
        return sumas;
    }

    private static Map<String, BigDecimal> ventasPorAnio(List<String> anios, Map<String, List<BigDecimal>> montos) {
        Map<String, BigDecimal> sumas = new HashMap<>();
        List<BigDecimal> totales = new ArrayList<>();
        totales.addAll(montos.get("Precio Unitario Producto/Servicio (USD/unidad)"));
        List<BigDecimal> monto = montos.get("Cantidad de ventas de Producto/Servicio (Unidades)");
        for (int i = 0; i < totales.size(); i++){
            totales.set(i, totales.get(i).multiply(monto.get(i).setScale(2, RoundingMode.HALF_UP)));
            sumas.put(anios.get(i),totales.get(i));
        }
        return sumas;
    }

    private static Map<String, List<BigDecimal>> extractData(Integer rI, Integer rF, Sheet sheet) {
        Map<String, List<BigDecimal>> data = new HashMap<>();
        for (int rw = rI; rw <= rF; rw++){
            Row row = sheet.getRow(rw);
            String gasto = row.getCell(0).getStringCellValue();
            List<BigDecimal> valores = new ArrayList<>();
            for (int cl = cellIni; cl <= cellFin; cl++){
                Cell cell = row.getCell(cl);
                if (cell.getCellType().equals(CellType.BLANK)){
                    valores.add(BigDecimal.ZERO);
                }else if (cell.getCellType().equals(CellType.NUMERIC)){
                    valores.add(BigDecimal.valueOf(cell.getNumericCellValue()).setScale(2,RoundingMode.HALF_UP));
                }else if (cell.getCellType().equals(CellType.STRING)){
                    valores.add(BigDecimal.valueOf(Long.parseLong(cell.getStringCellValue())));
                }else if (cell.getCellType().equals(CellType.FORMULA)){
                    valores.add(BigDecimal.valueOf(cell.getNumericCellValue()).setScale(2, RoundingMode.HALF_UP));
                }
            }
            data.put(gasto, valores);
        }
        return data;
    }
}
