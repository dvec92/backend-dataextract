package com.dv.ExtractData.models;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@Getter
@Setter
@NoArgsConstructor
public class DataExtract implements Serializable {
    String Titulo;
    String Periodos;
    String Moneda;
    Map<String, BigDecimal> inversion;
    List<String> aniosFlujoDeCaja;
    Map<String, List<BigDecimal>> ventas;
    Map<String, List<BigDecimal>> ingresos;
    Map<String, List<BigDecimal>> gastos;
    List<BigDecimal> totalGastos;
    Map<String, List<BigDecimal>> otrosRubros;
    List<BigDecimal> totalIngresos;
    List<BigDecimal> flujoCajaAntesImpuestos;
    List<BigDecimal> impuestoSobreRenta;
    List<BigDecimal> flujoNeto;
    List<BigDecimal> efectivoDisponible;
    Map<String, BigDecimal> clsificacionIngresos;
    Map<String, BigDecimal> ventasPorAnio;
}
